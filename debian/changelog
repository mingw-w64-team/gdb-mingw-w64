gdb-mingw-w64 (13.2) unstable; urgency=medium

  * Build for 64-bit UCRT.
  * Update Lintian overrides.
  * Standards-Version 4.7.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Fri, 24 Jan 2025 19:15:10 +0100

gdb-mingw-w64 (13.1) unstable; urgency=medium

  * Specify the target architecture when calculating build flags.
    Closes: #1063359.

 -- Stephen Kitt <skitt@debian.org>  Thu, 08 Feb 2024 19:15:03 +0100

gdb-mingw-w64 (13) unstable; urgency=medium

  * Switch from the obsolete libncurses5-dev build-dependency to
    libncurses-dev.
  * Build using the pthreads variant of g++-mingw-w64. Closes: #1052727.
    Build-depend only on the relevant packages.
  * Update Lintian overrides.
  * Standards-Version 4.6.2, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Fri, 29 Sep 2023 17:54:37 +0200

gdb-mingw-w64 (12) unstable; urgency=medium

  * Only build target gdbserver, not the full target gdb. Closes:
    #1008381.
  * Configure and build the components required for gdbserver
    explicitly; this avoids configuring gdb as a whole, and means we
    won’t end up requiring gmp for gdb 11. Closes: #1005734.
  * Standards-Version 4.6.0, no change required.
  * Update Lintian overrides (superfluous-file-pattern instead of
    wildcard-matches-nothing-in-dep5-copyright and unused-file-paragraph-
    in-dep5-copyright).

 -- Stephen Kitt <skitt@debian.org>  Tue, 29 Mar 2022 21:53:17 +0200

gdb-mingw-w64 (11) unstable; urgency=medium

  * Adjust to the changes in gdb 10. Closes: #975189.
  * Switch to debhelper compatibility level 13.

 -- Stephen Kitt <skitt@debian.org>  Thu, 19 Nov 2020 21:08:05 +0100

gdb-mingw-w64 (10.9) unstable; urgency=medium

  * MinGW-w64 now supports FORTIFY_SOURCE and SSP, and the former
    requires the latter. Because of this, the package no longer
    builds... Enabling SSP fixes this, but results in binaries depending
    on the SSP DLL, so build everything statically.
  * Switch to debhelper compatibility level 12.
  * Strip the target binaries.
  * Standards-Version 4.5.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 29 Jan 2020 21:48:00 +0100

gdb-mingw-w64 (10.8) unstable; urgency=medium

  * Update build dependencies. Closes: #921853.
  * Switch to Python 3.

 -- Stephen Kitt <skitt@debian.org>  Wed, 13 Feb 2019 22:30:54 +0100

gdb-mingw-w64 (10.7) unstable; urgency=medium

  * Force verbose builds.
  * Switch to secure URIs.
  * Update debian/copyright based on gdb-source’s.
  * Switch to debhelper compatibility level 11, and rewrite debian/rules
    in short dh style.
  * Add Lintian overrides for debian/copyright since it matches the
    upstream source, not the package contents.
  * Mark gdb-mingw-w64-target as “Multi-Arch: foreign”.
  * Set “Rules-Requires-Root: no”.
  * Fix a few spelling mistakes flagged by Lintian.
  * Standards-Version 4.3.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Fri, 08 Feb 2019 20:17:00 +0100

gdb-mingw-w64 (10.6) unstable; urgency=medium

  * Link the target binaries so they don’t need libstdc++ and libgcc DLLs
    at runtime. Thanks to Gianluigi Tiesi for the suggestion! Closes:
    #893562.
  * Migrate to Salsa.
  * Standards-Version 4.1.4, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 08 May 2018 23:07:01 +0200

gdb-mingw-w64 (10.5) unstable; urgency=medium

  * Disable format warnings/errors (the gdb build disables -Wformat on
    MinGW-w64 builds, but leaves -Werror=format-security, which breaks the
    build when combined with -Wno-format). Closes: #884242.
  * Switch to priority “optional”.
  * Enable all hardening options for Debian-native binaries.
  * Use pkg-info.mk instead of dpkg-parsechangelog.
  * Use architecture.mk instead of redefining build variables.
  * Standards-Version 4.1.3, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Sun, 07 Jan 2018 17:28:32 +0100

gdb-mingw-w64 (10.4) unstable; urgency=medium

  * Disable package mangling in Launchpad.

 -- Stephen Kitt <skitt@debian.org>  Mon, 19 Sep 2016 20:36:37 +0200

gdb-mingw-w64 (10.3) unstable; urgency=medium

  * Suggest the non-free gdb-doc package since that's where gdb.1 is
    nowadays.
  * Switch to https: VCS URIs (see #810378).
  * Build-depend on g++-mingw-w64; versions of gdb after 7.11 need a C++
    compiler (this should fix the Ubuntu build failure).
  * Clean up debian/control with cme.
  * Standards-Version 3.9.8, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sun, 18 Sep 2016 15:24:11 +0200

gdb-mingw-w64 (10.2) unstable; urgency=medium

  * Build using the system-provided zlib1g (and build-depend on
    zlib1g-dev).
  * Switch to debhelper compatibility level 9.
  * Build-depend on libbabeltrace-ctf-dev and libbabeltrace-dev to
    support CTF files.
  * Build-depend on gdb-source 7.10 or later so the builds are at least
    equivalent to the last binNMU.

 -- Stephen Kitt <skitt@debian.org>  Mon, 28 Dec 2015 23:18:32 +0100

gdb-mingw-w64 (10.1) unstable; urgency=medium

  * Build-depend on mig on hurd.

 -- Stephen Kitt <skitt@debian.org>  Sun, 31 May 2015 22:55:47 +0200

gdb-mingw-w64 (10) unstable; urgency=medium

  * Disable PE timestamps in the generated Windows executables; this
    allows the package to build reproducibly.
  * Update licensing information.
  * Standards-Version 3.9.6, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 30 May 2015 22:56:30 +0200

gdb-mingw-w64 (9) unstable; urgency=medium

  * gdb version 7.7 no longer ships target-specific manpages; instead of
    shipping the built versions, link to gdb's manpage. (This allows
    building on Ubuntu.)
  * Build-depend on libcloog-isl-dev instead of libcloog-ppl-dev.

 -- Stephen Kitt <skitt@debian.org>  Fri, 30 May 2014 22:30:41 +0200

gdb-mingw-w64 (8) unstable; urgency=medium

  * Avoid using "-Werror" (Closes: #746850).
  * Standards-Version 3.9.5, no change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 07 May 2014 07:13:30 +0200

gdb-mingw-w64 (7) unstable; urgency=low

  * Drop unused, target-specific build-dependencies (in particular
    libunwind7-dev to help the libunwind transition).

 -- Stephen Kitt <skitt@debian.org>  Sat, 29 Jun 2013 17:09:06 +0200

gdb-mingw-w64 (6) unstable; urgency=low

  * gdb no longer builds gdbtui, so stop trying to install it.
  * Switch to my Debian address.
  * Drop "DM-Upload-Allowed".
  * Use canonical VCS URIs.
  * Standards-Version 3.9.4, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sun, 09 Jun 2013 13:27:29 +0200

gdb-mingw-w64 (5) unstable; urgency=low

  * Drop gdb-spelling-fixes.patch completely now that 7.4 is in Debian;
    build-depend on gdb-source 7.4 or later.
  * Use "-a" and "-i" instead of listing architecture-dependent and
    -independent packages explicitly.
  * Standards-Version 3.9.3, no change required.
  * wrap-and-sort control files.
  * Export dpkg-buildflags' settings when building as well as configuring
    so the fortified functions end up being used correctly.
  * Build-depend on gcc-mingw-w64 rather than mingw-w64, we only need a C
    compiler.
  * Update debian/copyright format URL and years.

 -- Stephen Kitt <steve@sk2.org>  Thu, 24 May 2012 23:23:45 +0200

gdb-mingw-w64 (4) unstable; urgency=low

  * gdb-spelling-fixes.patch is applied upstream in version 7.4, so
    avoid failing if the patch is already applied (LP: #935097).

 -- Stephen Kitt <steve@sk2.org>  Sat, 18 Feb 2012 22:56:24 +0100

gdb-mingw-w64 (3) unstable; urgency=low

  [ Stephen Kitt ]
  * Disable stackprotector, relro, bindnow and pie dpkg-buildflags
    hardening features for Windows-targeted builds (LP: #891102).

  [ Didier Raboud ]
  * Confidently setting the DM-Upload-Allowed flag to yes.

 -- Stephen Kitt <steve@sk2.org>  Tue, 22 Nov 2011 18:06:28 +0100

gdb-mingw-w64 (2) unstable; urgency=low

  * Make the build target only depend on build-arch, so that the build
    target no longer requires Build-Depends-Indep dependencies; the binary
    target still does the right thing so the "Architecture: all" package
    can be built.

 -- Stephen Kitt <steve@sk2.org>  Thu, 29 Sep 2011 00:07:05 +0200

gdb-mingw-w64 (1) unstable; urgency=low

  * Split off "Architecture: all" package.
  * Switch to single-number versions and add the gdb-mingw-w64 version to
    the resulting binary package's version.
  * Refresh gdb-spelling-fixes.patch (Closes: #643556); build-depend on
    gdb-source 7.3 or later as a result.

 -- Stephen Kitt <steve@sk2.org>  Tue, 27 Sep 2011 23:36:02 +0200

gdb-mingw-w64 (0.1) unstable; urgency=low

  * Initial release (Closes: #570255, #626588).

 -- Stephen Kitt <steve@sk2.org>  Thu, 12 May 2011 20:48:48 +0200
